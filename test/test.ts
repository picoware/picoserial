import { AutoSerializer } from "@picoware/picoserial"

class Titi {
    a!: string;
    b!: string;

    static serializer = new AutoSerializer<Titi>(
        Titi,
        {
            id: "Titi",
            props: {
                a: { type: "string" },
                b: { type: "string" }
            }
        }
    )
}

class Tutu {
    value!: number
    static serializer = new AutoSerializer<Tutu>(
        Tutu,
        {
            id: "Tutu",
            props: {
                value: { type: "number" }
            }
        }
    )
}

class Toto {
    titi!: number[];

    static serializer = new AutoSerializer<Toto>(
        Toto,
        {
            id: "Toto",
            props: {
                // titi: { array: { ref: "Titi" } }
                titi: { array: Titi.serializer.schema },
                tata: { map: { type: "number" } },
                tutu: { map: Tutu.serializer.schema }
            }
        }
    )
}

const totoData = {
    titi: [
        {
            a: "a",
            b: "b",
        },
        {
            a: "A",
            b: "B",
        }
    ],
    tata: {
        0: 1,
        25: 2,
    },

    tutu: {
        key1: { value: 1 },
        key2: { value: 2 }
    }
}

const toto = Toto.serializer.fromData(totoData)

console.log(totoData)
console.log(toto)
console.log(Toto.serializer.toData(toto))

