import * as esbuild from 'esbuild'

await esbuild.build({
    entryPoints: ['src/index.ts'],
    bundle: true,
    packages: 'external',
    platform: 'neutral',
    target: 'esnext',
    sourcemap: true,
    outdir: 'dist',
})

console.log("Build succeeded.")