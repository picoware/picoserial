import type { NamedSchema } from "./schema";
import { Validator } from "./validator";

export class Serializer<T> extends Validator {

    _ctor: new () => T;
    _fromDataApply: { (data: object, instance: T, params?: any): void } = () => { throw "Serializer: fromDataApply not implemented" }
    _toData: { (instance: T): object } = () => { throw "Serializer: toData not implemented" }

    constructor(
        ctor: new () => T,
        schema: NamedSchema,
        fromDataApply?: { (data: object, instance: T, params?: any): void },
        toData?: { (instance: T): object }
    ) {
        super(schema);
        this._ctor = ctor;
        if (fromDataApply) this._fromDataApply = fromDataApply;
        if (toData) this._toData = toData;
    }

    fromDataApply(data: object, instance: T, params?: any): void {
        this._fromDataApply(data, instance, params);
    }

    fromData(data: object, params?: any): T {
        this.validate(data);
        const instance = new this._ctor();
        this._fromDataApply(data, instance, params);
        return instance;
    }

    toData(instance: T): object {
        const data = this._toData(instance);
        this.validate(data);
        return data;
    }
}

export class AsyncSerializer<T> extends Validator {

    #ctor: new () => T;
    #fromDataApply: { (data: object, instance: T, params?: any): Promise<void> } = () => { throw "AsyncSerializer: fromDataApply not implemented" }
    #toData: { (instance: T): Promise<object> } = () => { throw "AsyncSerializer: toData not implemented" }

    constructor(
        ctor: new () => T,
        schema: NamedSchema,
        fromDataApply?: { (data: object, instance: T, params?: any): Promise<void> },
        toData?: { (instance: T): Promise<object> }
    ) {
        super(schema);
        this.#ctor = ctor;
        if (fromDataApply) this.#fromDataApply = fromDataApply;
        if (toData) this.#toData = toData;
    }

    async fromDataApply(data: object, instance: T, params?: any): Promise<void> {
        await this.#fromDataApply(data, instance, params);
    }

    async fromData(data: object, params?: any): Promise<T> {
        this.validate(data);
        const instance = new this.#ctor();
        await this.#fromDataApply(data, instance, params);
        return instance;
    }

    async toData(instance: T): Promise<object> {
        const data = await this.#toData(instance);
        this.validate(data);
        return data;
    }
}



