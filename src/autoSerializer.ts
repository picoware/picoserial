import type { Schema, ClassSchema } from "./schema";
import { Serializer } from "./serializer";

type AutoFieldsKeys<T> = {
    [K in keyof T]:
    T[K] extends Function ? never :
    T[K] extends AutoSerializer<T> ? never :
    K
}[keyof T]

export type AutoFields<T> = {
    [K in AutoFieldsKeys<T>]?: T[K]
}

export class AutoSerializer<T> extends Serializer<T> {

    declare schema: ClassSchema;

    constructor(ctor: new () => T, schema: ClassSchema) {
        super(ctor, schema);
    }

    init(data: AutoFields<T>, instance: T) {
        Object.assign(instance as object, data);
    }

    _fromDataApply = (data: AutoFields<T>, instance: T): void => {
        this._recFromData(instance, data, this.schema);
    }

    _toData = (instance: T): AutoFields<T> => {
        const keys = Object.keys(instance as object).filter((key: string) => typeof (instance as { [key: string]: any })[key] != "function")
        const data: { [key: string]: any } = {}
        keys.forEach(key => data[key] = this._recToData((instance as { [key: string]: any })[key]))
        return data as AutoFields<T>;
    }

    _recFromData(instance: T, data: any, schema: Schema): any {
        if (schema.id && schema.id == this.id) {
            const keys = Object.keys(data);
            const parsedData: { [key: string]: any } = {};
            keys.forEach(key => {
                parsedData[key] = this._recFromData(instance, data[key], schema.props![key])
            })
            this.init(parsedData as AutoFields<T>, instance);
            return instance;
        }

        if (schema.id) {
            return (AutoSerializer.map[schema.id] as AutoSerializer<T>).fromData(data)
        }

        if (schema.ref) {
            return (AutoSerializer.map[schema.ref] as AutoSerializer<T>).fromData(data)
        }

        if (schema.array) {
            return data.map((elem: any) => this._recFromData(instance, elem, schema.array!))
        }

        if (schema.map) {
            const keys = Object.keys(data);
            const result: { [key: string]: any } = {};
            keys.forEach(key => {
                result[key] = this._recFromData(instance, data[key], schema.map!)
            })
            return result
        }

        if (schema.props) {
            const keys = Object.keys(data);
            const result: { [key: string]: any } = {};
            keys.forEach(key => {
                result[key] = this._recFromData(instance, data[key], schema.props![key])
            })
            return result
        }
        return data;
    }



    _recToData(instanceData: any): any {
        if (instanceData.serializer instanceof AutoSerializer) {
            return instanceData.serializer.toData(instanceData)
        }
        if (instanceData instanceof Array) {
            return instanceData.map(elem => this._recToData(elem))
        }
        if (instanceData instanceof Object) {
            const keys = Object.keys(instanceData).filter(key => typeof instanceData[key] != "function");
            const result: { [key: string]: any } = {};
            keys.forEach(key => {
                result[key] = this._recToData(instanceData[key])
            })
            return result
        }
        return instanceData;
    }
}
