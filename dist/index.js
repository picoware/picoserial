// src/validator.ts
var Validator = class _Validator {
  schema;
  id;
  static map = {};
  constructor(schema) {
    this.schema = schema;
    this.id = schema.id;
    if (_Validator.map[this.id])
      throw `Validator: id ${this.id} is already defined`;
    _Validator.map[this.id] = this;
  }
  validate(data) {
    this._recValidate(data, this.schema);
  }
  _recValidate(data, schema) {
    let xorProp = 0;
    if (schema.id && schema.id != this.id) {
      return;
    }
    if (schema.values) {
      xorProp++;
      if (xorProp > 1)
        throw "Validator: too many props defined on schema.";
      if (!schema.values.includes(data))
        throw `Validator: data ${data} should be one of ${schema.values}`;
    }
    if (schema.type) {
      xorProp++;
      if (xorProp > 1)
        throw "Validator: too many props defined on schema.";
      if (typeof data !== schema.type)
        throw `Validator: data ${data} is not of type ${schema.type}`;
    }
    if (schema.array) {
      xorProp++;
      if (xorProp > 1)
        throw "Validator: too many props defined on schema.";
      if (!(data instanceof Array))
        throw `Validator: data ${data} is not of type ${schema.type}`;
      data.forEach((datum) => this._recValidate(datum, schema.array));
    }
    if (schema.map) {
      xorProp++;
      if (xorProp > 1)
        throw "Validator: too many props defined on schema.";
      const keys = Object.keys(data);
      keys.forEach((key) => {
        this._recValidate(data[key], schema.map);
      });
    }
    if (schema.props || schema.optionalProps) {
      xorProp++;
      if (xorProp > 1)
        throw "Validator: too many props defined on schema.";
      const dataProps = Object.keys(data);
      const props = schema.props ? Object.keys(schema.props) : [];
      const optionalProps = schema.optionalProps ? Object.keys(schema.optionalProps) : [];
      dataProps.forEach((prop) => {
        if (!props.includes(prop) && !optionalProps.includes(prop))
          throw `Validator: prop ${prop} is not defined in schema ${schema}`;
      });
      props.forEach((prop) => {
        if (!dataProps.includes(prop))
          throw `Validator: prop ${prop} is missing in data ${data}`;
      });
      const allProps = { ...schema.props, ...schema.optionalProps };
      dataProps.forEach((propName) => {
        this._recValidate(data[propName], allProps[propName]);
      });
    }
    if (schema.constraints) {
      schema.constraints.forEach((constraint) => {
        if (!constraint(data))
          throw `Validator: data ${data} should respect constraint ${constraint}`;
      });
    }
  }
};

// src/serializer.ts
var Serializer = class extends Validator {
  _ctor;
  _fromDataApply = () => {
    throw "Serializer: fromDataApply not implemented";
  };
  _toData = () => {
    throw "Serializer: toData not implemented";
  };
  constructor(ctor, schema, fromDataApply, toData) {
    super(schema);
    this._ctor = ctor;
    if (fromDataApply)
      this._fromDataApply = fromDataApply;
    if (toData)
      this._toData = toData;
  }
  fromDataApply(data, instance, params) {
    this._fromDataApply(data, instance, params);
  }
  fromData(data, params) {
    this.validate(data);
    const instance = new this._ctor();
    this._fromDataApply(data, instance, params);
    return instance;
  }
  toData(instance) {
    const data = this._toData(instance);
    this.validate(data);
    return data;
  }
};
var AsyncSerializer = class extends Validator {
  #ctor;
  #fromDataApply = () => {
    throw "AsyncSerializer: fromDataApply not implemented";
  };
  #toData = () => {
    throw "AsyncSerializer: toData not implemented";
  };
  constructor(ctor, schema, fromDataApply, toData) {
    super(schema);
    this.#ctor = ctor;
    if (fromDataApply)
      this.#fromDataApply = fromDataApply;
    if (toData)
      this.#toData = toData;
  }
  async fromDataApply(data, instance, params) {
    await this.#fromDataApply(data, instance, params);
  }
  async fromData(data, params) {
    this.validate(data);
    const instance = new this.#ctor();
    await this.#fromDataApply(data, instance, params);
    return instance;
  }
  async toData(instance) {
    const data = await this.#toData(instance);
    this.validate(data);
    return data;
  }
};

// src/autoSerializer.ts
var AutoSerializer = class _AutoSerializer extends Serializer {
  constructor(ctor, schema) {
    super(ctor, schema);
  }
  init(data, instance) {
    Object.assign(instance, data);
  }
  _fromDataApply = (data, instance) => {
    this._recFromData(instance, data, this.schema);
  };
  _toData = (instance) => {
    const keys = Object.keys(instance).filter((key) => typeof instance[key] != "function");
    const data = {};
    keys.forEach((key) => data[key] = this._recToData(instance[key]));
    return data;
  };
  _recFromData(instance, data, schema) {
    if (schema.id && schema.id == this.id) {
      const keys = Object.keys(data);
      const parsedData = {};
      keys.forEach((key) => {
        parsedData[key] = this._recFromData(instance, data[key], schema.props[key]);
      });
      this.init(parsedData, instance);
      return instance;
    }
    if (schema.id) {
      return _AutoSerializer.map[schema.id].fromData(data);
    }
    if (schema.ref) {
      return _AutoSerializer.map[schema.ref].fromData(data);
    }
    if (schema.array) {
      return data.map((elem) => this._recFromData(instance, elem, schema.array));
    }
    if (schema.map) {
      const keys = Object.keys(data);
      const result = {};
      keys.forEach((key) => {
        result[key] = this._recFromData(instance, data[key], schema.map);
      });
      return result;
    }
    if (schema.props) {
      const keys = Object.keys(data);
      const result = {};
      keys.forEach((key) => {
        result[key] = this._recFromData(instance, data[key], schema.props[key]);
      });
      return result;
    }
    return data;
  }
  _recToData(instanceData) {
    if (instanceData.serializer instanceof _AutoSerializer) {
      return instanceData.serializer.toData(instanceData);
    }
    if (instanceData instanceof Array) {
      return instanceData.map((elem) => this._recToData(elem));
    }
    if (instanceData instanceof Object) {
      const keys = Object.keys(instanceData).filter((key) => typeof instanceData[key] != "function");
      const result = {};
      keys.forEach((key) => {
        result[key] = this._recToData(instanceData[key]);
      });
      return result;
    }
    return instanceData;
  }
};
export {
  AsyncSerializer,
  AutoSerializer,
  Serializer,
  Validator
};
//# sourceMappingURL=index.js.map
