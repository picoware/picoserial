import type { NamedSchema } from "./schema";
import { Validator } from "./validator";
export declare class Serializer<T> extends Validator {
    _ctor: new () => T;
    _fromDataApply: {
        (data: object, instance: T, params?: any): void;
    };
    _toData: {
        (instance: T): object;
    };
    constructor(ctor: new () => T, schema: NamedSchema, fromDataApply?: {
        (data: object, instance: T, params?: any): void;
    }, toData?: {
        (instance: T): object;
    });
    fromDataApply(data: object, instance: T, params?: any): void;
    fromData(data: object, params?: any): T;
    toData(instance: T): object;
}
export declare class AsyncSerializer<T> extends Validator {
    #private;
    constructor(ctor: new () => T, schema: NamedSchema, fromDataApply?: {
        (data: object, instance: T, params?: any): Promise<void>;
    }, toData?: {
        (instance: T): Promise<object>;
    });
    fromDataApply(data: object, instance: T, params?: any): Promise<void>;
    fromData(data: object, params?: any): Promise<T>;
    toData(instance: T): Promise<object>;
}
//# sourceMappingURL=serializer.d.ts.map