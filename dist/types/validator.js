export class Validator {
    schema;
    id;
    static map = {};
    constructor(schema) {
        this.schema = schema;
        this.id = schema.id;
        if (Validator.map[this.id])
            throw `Validator: id ${this.id} is already defined`;
        Validator.map[this.id] = this;
    }
    validate(data) {
        this._recValidate(data, this.schema);
    }
    _recValidate(data, schema) {
        let xorProp = 0;
        if (schema.id && schema.id != this.id) {
            return;
        }
        if (schema.values) {
            xorProp++;
            if (xorProp > 1)
                throw "Validator: too many props defined on schema.";
            if (!schema.values.includes(data))
                throw `Validator: data ${data} should be one of ${schema.values}`;
        }
        if (schema.type) {
            xorProp++;
            if (xorProp > 1)
                throw "Validator: too many props defined on schema.";
            if (typeof data !== schema.type)
                throw `Validator: data ${data} is not of type ${schema.type}`;
        }
        if (schema.array) {
            xorProp++;
            if (xorProp > 1)
                throw "Validator: too many props defined on schema.";
            if (!(data instanceof Array))
                throw `Validator: data ${data} is not of type ${schema.type}`;
            data.forEach(datum => this._recValidate(datum, schema.array));
        }
        if (schema.map) {
            xorProp++;
            if (xorProp > 1)
                throw "Validator: too many props defined on schema.";
            const keys = Object.keys(data);
            keys.forEach(key => {
                this._recValidate(data[key], schema.map);
            });
        }
        if (schema.props || schema.optionalProps) {
            xorProp++;
            if (xorProp > 1)
                throw "Validator: too many props defined on schema.";
            const dataProps = Object.keys(data);
            const props = schema.props ? Object.keys(schema.props) : [];
            const optionalProps = schema.optionalProps ? Object.keys(schema.optionalProps) : [];
            dataProps.forEach(prop => {
                if (!props.includes(prop) && !optionalProps.includes(prop))
                    throw `Validator: prop ${prop} is not defined in schema ${schema}`;
            });
            props.forEach(prop => {
                if (!dataProps.includes(prop))
                    throw `Validator: prop ${prop} is missing in data ${data}`;
            });
            const allProps = { ...schema.props, ...schema.optionalProps };
            dataProps.forEach(propName => {
                this._recValidate(data[propName], allProps[propName]);
            });
        }
        if (schema.constraints) {
            schema.constraints.forEach(constraint => {
                if (!constraint(data))
                    throw `Validator: data ${data} should respect constraint ${constraint}`;
            });
        }
    }
}
