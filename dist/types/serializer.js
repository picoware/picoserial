import { Validator } from "./validator";
export class Serializer extends Validator {
    _ctor;
    _fromDataApply = () => { throw "Serializer: fromDataApply not implemented"; };
    _toData = () => { throw "Serializer: toData not implemented"; };
    constructor(ctor, schema, fromDataApply, toData) {
        super(schema);
        this._ctor = ctor;
        if (fromDataApply)
            this._fromDataApply = fromDataApply;
        if (toData)
            this._toData = toData;
    }
    fromDataApply(data, instance, params) {
        this._fromDataApply(data, instance, params);
    }
    fromData(data, params) {
        this.validate(data);
        const instance = new this._ctor();
        this._fromDataApply(data, instance, params);
        return instance;
    }
    toData(instance) {
        const data = this._toData(instance);
        this.validate(data);
        return data;
    }
}
export class AsyncSerializer extends Validator {
    #ctor;
    #fromDataApply = () => { throw "AsyncSerializer: fromDataApply not implemented"; };
    #toData = () => { throw "AsyncSerializer: toData not implemented"; };
    constructor(ctor, schema, fromDataApply, toData) {
        super(schema);
        this.#ctor = ctor;
        if (fromDataApply)
            this.#fromDataApply = fromDataApply;
        if (toData)
            this.#toData = toData;
    }
    async fromDataApply(data, instance, params) {
        await this.#fromDataApply(data, instance, params);
    }
    async fromData(data, params) {
        this.validate(data);
        const instance = new this.#ctor();
        await this.#fromDataApply(data, instance, params);
        return instance;
    }
    async toData(instance) {
        const data = await this.#toData(instance);
        this.validate(data);
        return data;
    }
}
