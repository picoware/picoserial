export interface Schema {
    id?: string;
    ref?: string;
    type?: "boolean" | "number" | "string";
    values?: Array<number | string>;
    array?: Schema;
    map?: Schema;
    props?: {
        [key: string]: Schema;
    };
    optionalProps?: {
        [key: string]: Schema;
    };
    constraints?: Array<(data: any) => boolean>;
}
export interface NamedSchema extends Schema {
    id: string;
}
export interface ClassSchema extends NamedSchema {
    ref?: never;
    type?: never;
    values?: never;
    array?: never;
    map?: never;
}
//# sourceMappingURL=schema.d.ts.map