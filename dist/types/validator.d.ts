import type { NamedSchema, Schema } from "./schema";
export declare class Validator {
    schema: NamedSchema;
    id: string;
    static map: {
        [id: string]: Validator;
    };
    constructor(schema: NamedSchema);
    validate(data: any): void;
    _recValidate(data: any, schema: Schema): void;
}
//# sourceMappingURL=validator.d.ts.map