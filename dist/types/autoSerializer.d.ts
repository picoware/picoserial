import type { Schema, ClassSchema } from "./schema";
import { Serializer } from "./serializer";
type AutoFieldsKeys<T> = {
    [K in keyof T]: T[K] extends Function ? never : T[K] extends AutoSerializer<T> ? never : K;
}[keyof T];
export type AutoFields<T> = {
    [K in AutoFieldsKeys<T>]?: T[K];
};
export declare class AutoSerializer<T> extends Serializer<T> {
    schema: ClassSchema;
    constructor(ctor: new () => T, schema: ClassSchema);
    init(data: AutoFields<T>, instance: T): void;
    _fromDataApply: (data: AutoFields<T>, instance: T) => void;
    _toData: (instance: T) => AutoFields<T>;
    _recFromData(instance: T, data: any, schema: Schema): any;
    _recToData(instanceData: any): any;
}
export {};
//# sourceMappingURL=autoSerializer.d.ts.map