import { Serializer } from "./serializer";
export class AutoSerializer extends Serializer {
    constructor(ctor, schema) {
        super(ctor, schema);
    }
    init(data, instance) {
        Object.assign(instance, data);
    }
    _fromDataApply = (data, instance) => {
        this._recFromData(instance, data, this.schema);
    };
    _toData = (instance) => {
        const keys = Object.keys(instance).filter((key) => typeof instance[key] != "function");
        const data = {};
        keys.forEach(key => data[key] = this._recToData(instance[key]));
        return data;
    };
    _recFromData(instance, data, schema) {
        if (schema.id && schema.id == this.id) {
            const keys = Object.keys(data);
            const parsedData = {};
            keys.forEach(key => {
                parsedData[key] = this._recFromData(instance, data[key], schema.props[key]);
            });
            this.init(parsedData, instance);
            return instance;
        }
        if (schema.id) {
            return AutoSerializer.map[schema.id].fromData(data);
        }
        if (schema.ref) {
            return AutoSerializer.map[schema.ref].fromData(data);
        }
        if (schema.array) {
            return data.map((elem) => this._recFromData(instance, elem, schema.array));
        }
        if (schema.map) {
            const keys = Object.keys(data);
            const result = {};
            keys.forEach(key => {
                result[key] = this._recFromData(instance, data[key], schema.map);
            });
            return result;
        }
        if (schema.props) {
            const keys = Object.keys(data);
            const result = {};
            keys.forEach(key => {
                result[key] = this._recFromData(instance, data[key], schema.props[key]);
            });
            return result;
        }
        return data;
    }
    _recToData(instanceData) {
        if (instanceData.serializer instanceof AutoSerializer) {
            return instanceData.serializer.toData(instanceData);
        }
        if (instanceData instanceof Array) {
            return instanceData.map(elem => this._recToData(elem));
        }
        if (instanceData instanceof Object) {
            const keys = Object.keys(instanceData).filter(key => typeof instanceData[key] != "function");
            const result = {};
            keys.forEach(key => {
                result[key] = this._recToData(instanceData[key]);
            });
            return result;
        }
        return instanceData;
    }
}
